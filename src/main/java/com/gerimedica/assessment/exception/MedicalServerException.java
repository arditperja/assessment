package com.gerimedica.assessment.exception;

public class MedicalServerException extends RuntimeException {

    public static final String INTERNAL_ERROR_MESSAGE = "Impossible to proceed due to an internal error!";

    public MedicalServerException(final String message) {
        super(message);
    }
}
