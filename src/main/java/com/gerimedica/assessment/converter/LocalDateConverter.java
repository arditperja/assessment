package com.gerimedica.assessment.converter;

import liquibase.repackaged.com.opencsv.bean.AbstractBeanField;
import liquibase.repackaged.org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class LocalDateConverter extends AbstractBeanField {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("MM-dd-yyyy");

    @Override
    protected Object convert(String source) {
        if (StringUtils.isBlank(source)) {
            return null;
        }

        return LocalDate.parse(source, FORMATTER);
    }
}
