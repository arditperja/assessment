package com.gerimedica.assessment.dto.response;

import com.gerimedica.assessment.dto.MedicalDetailDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.LinkedList;
import java.util.List;


@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode
@ApiModel(value = "The medical detail view model")
public class MedicalDetailListDTOResponse {

    @ApiModelProperty(value = "All Medical details")
    private List<MedicalDetailDTO> medicalDetails = new LinkedList<>();

}
