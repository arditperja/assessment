package com.gerimedica.assessment.service;

import com.gerimedica.assessment.domain.MedicalDetail;
import com.gerimedica.assessment.dto.MedicalDetailDTO;
import com.gerimedica.assessment.dto.response.BasicDTOResponse;
import com.gerimedica.assessment.dto.response.MedicalDetailListDTOResponse;
import com.gerimedica.assessment.dto.response.SingleMedicalDetailDTOResponse;
import com.gerimedica.assessment.mapper.MedicalDetailMapper;
import com.gerimedica.assessment.respository.MedicalDetailRepository;
import liquibase.repackaged.com.opencsv.bean.CsvToBeanBuilder;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@AllArgsConstructor
public class MedicalService {

    private final MedicalDetailRepository checkupRepository;
    private final MedicalDetailMapper checkupMapper;

    /**
     * Upload the csv file into database. It will skip empty lines.
     *
     * @param bufferedReader {@link BufferedReader}
     * @return {@link BasicDTOResponse}
     */
    public BasicDTOResponse saveUploadedFile(final BufferedReader bufferedReader) {
        final List<MedicalDetailDTO> medicalDetails = new CsvToBeanBuilder<MedicalDetailDTO>(bufferedReader)
                .withType(MedicalDetailDTO.class)
                .withIgnoreEmptyLine(true)
                .withSkipLines(1)
                .build()
                .parse();

        final List<MedicalDetail> toBeSaved = checkupMapper.medicalCheckupDTOToMedicalCheckups(medicalDetails);
        final Set<String> medicalInError = new HashSet<>();
        final Set<String> medicalInSuccess = new HashSet<>();
        for (final MedicalDetail medicalDetail : toBeSaved) {
            try {
                checkupRepository.save(medicalDetail);
                medicalInSuccess.add(medicalDetail.getCode());
            } catch (Exception ex) {
                medicalInError.add(medicalDetail.getCode());
            }
        }

        return BasicDTOResponse.builder().message(String.format("Uploaded file data saved successfully! \n"
                        + "Medical Detail In Success: %s "
                        + "Medical Detail In Error: %s [%s]",
                medicalInSuccess.size(), medicalInError.size(), String.join(";", medicalInError)))
                .build();
    }

    /**
     * Return all the medical details present on the database.
     *
     * @return {@link MedicalDetailListDTOResponse}
     */
    public MedicalDetailListDTOResponse getAllMedicalDetails() {
        final List<MedicalDetail> allMedicalDetails = checkupRepository.findAll();
        return MedicalDetailListDTOResponse.builder()
                .medicalDetails(checkupMapper.medicalCheckupToMedicalCheckupDTOs(allMedicalDetails)).build();
    }

    /**
     * Return a single specific medical details based on the specific code.
     *
     * @param code the unique medical detail code
     * @return {@link SingleMedicalDetailDTOResponse}
     */
    public SingleMedicalDetailDTOResponse getSpecificMedicalDetail(final String code) {
        final MedicalDetail specificMedicalDetail = checkupRepository.findTopByCode(code);
        return SingleMedicalDetailDTOResponse.builder()
                .medicalDetail(checkupMapper.medicalCheckupToMedicalCheckupDTO(specificMedicalDetail)).build();
    }

    /**
     * Delete all the medical details present on the database.
     *
     * @return {@link BasicDTOResponse}
     */
    public BasicDTOResponse deleteAll() {
        checkupRepository.deleteAll();
        return BasicDTOResponse.builder()
                .message("Medical Details deleted successfully!").build();
    }

}
