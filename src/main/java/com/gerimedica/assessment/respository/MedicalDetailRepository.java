package com.gerimedica.assessment.respository;

import com.gerimedica.assessment.domain.MedicalDetail;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicalDetailRepository extends CrudRepository<MedicalDetail, Long> {

    List<MedicalDetail> findAll();

    MedicalDetail findTopByCode(final String code);
}
