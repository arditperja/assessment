package com.gerimedica.assessment.web;

import com.gerimedica.assessment.AssessmentApplication;
import com.gerimedica.assessment.domain.MedicalDetail;
import com.gerimedica.assessment.respository.MedicalDetailRepository;
import com.gerimedica.assessment.web.errors.ExceptionTranslator;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.io.InputStream;
import java.util.List;


@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@TestPropertySource(locations = "classpath:application-test.properties")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = AssessmentApplication.class)
public class MedicalResourceTest {

    private final static String CSV_FILE_NAME = "exercise.csv";
    private final static String CSV_FILE_PATH = "data/" + CSV_FILE_NAME;
    private final static String SPECIFIC_MEDICAL_DETAIL_CODE = "271636001";
    private final static String DELETE_SUCCESS_MESSAGE = "Medical Details deleted successfully!";

    @Autowired
    private MedicalResource medicalResource;

    @Autowired
    private MedicalDetailRepository medicalRepository;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        MockitoAnnotations.openMocks(this);

        this.mockMvc = MockMvcBuilders
                .standaloneSetup(medicalResource)
                .setControllerAdvice(new ExceptionTranslator())
                .build();
    }

    @Test()
    public void test1_successfully_upload_file_and_retrieve_data() throws Exception {
        final MockMultipartFile file = new MockMultipartFile("file", CSV_FILE_NAME,
                MediaType.MULTIPART_FORM_DATA_VALUE, getFileAsIOStream().readAllBytes());
        mockMvc.perform(MockMvcRequestBuilders.multipart("/medical/upload").file(file))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void test2_retrieve_medical_details() throws Exception {
        final List<MedicalDetail> medicalDetails = medicalRepository.findAll();

        mockMvc.perform(MockMvcRequestBuilders.get("/medical/details"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("medicalDetails", Matchers.hasSize(medicalDetails.size())));
    }

    @Test
    public void test3_retrieve_specific_medical_detail() throws Exception {
        final MedicalDetail medicalDetail = medicalRepository.findTopByCode(SPECIFIC_MEDICAL_DETAIL_CODE);

        mockMvc.perform(MockMvcRequestBuilders.get(String.join("", "/medical/details/", SPECIFIC_MEDICAL_DETAIL_CODE)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("medicalDetail.code", Matchers.equalTo(medicalDetail.getCode())));
    }

    @Test
    public void test4_delete_all_medical_details() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/medical/wipe"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("message", Matchers.equalTo(DELETE_SUCCESS_MESSAGE)));
    }

    private InputStream getFileAsIOStream() {
        return this.getClass()
                .getClassLoader()
                .getResourceAsStream(CSV_FILE_PATH);
    }
}
