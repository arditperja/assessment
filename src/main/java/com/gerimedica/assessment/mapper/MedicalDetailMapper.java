package com.gerimedica.assessment.mapper;

import com.gerimedica.assessment.domain.MedicalDetail;
import com.gerimedica.assessment.dto.MedicalDetailDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper
public interface MedicalDetailMapper {


    MedicalDetailDTO medicalCheckupToMedicalCheckupDTO(MedicalDetail medicalDetail);

    List<MedicalDetailDTO> medicalCheckupToMedicalCheckupDTOs(List<MedicalDetail> medicalDetails);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdDate", ignore = true)
    MedicalDetail medicalCheckupDTOToMedicalCheckup(MedicalDetailDTO medicalDetailDTO);

    List<MedicalDetail> medicalCheckupDTOToMedicalCheckups(List<MedicalDetailDTO> medicalDetailDTOS);
}
