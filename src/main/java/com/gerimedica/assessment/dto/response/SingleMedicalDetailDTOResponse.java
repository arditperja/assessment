package com.gerimedica.assessment.dto.response;

import com.gerimedica.assessment.dto.MedicalDetailDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;


@Data
@SuperBuilder
@NoArgsConstructor
@ApiModel(value = "The medical detail view model")
public class SingleMedicalDetailDTOResponse {

    @ApiModelProperty(value = "Medical details")
    private MedicalDetailDTO medicalDetail;

}
