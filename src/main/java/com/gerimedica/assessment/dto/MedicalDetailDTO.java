package com.gerimedica.assessment.dto;

import com.gerimedica.assessment.converter.LocalDateConverter;
import liquibase.repackaged.com.opencsv.bean.CsvBindByPosition;
import liquibase.repackaged.com.opencsv.bean.CsvCustomBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MedicalDetailDTO {

    @CsvBindByPosition(position = 0)
    private String source;

    @CsvBindByPosition(position = 1)
    private String codeListCode;

    @CsvBindByPosition(position = 2)
    private String code;

    @CsvBindByPosition(position = 3)
    private String displayValue;

    @CsvBindByPosition(position = 4)
    private String longDescription;

    @CsvCustomBindByPosition(position = 5, converter = LocalDateConverter.class)
    private LocalDate fromDate;

    @CsvCustomBindByPosition(position = 6, converter = LocalDateConverter.class)
    private LocalDate toDate;

    @CsvBindByPosition(position = 7)
    private Integer sortingPriority;

}
