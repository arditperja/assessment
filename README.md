# CSV Manipulator data REST API

Simple application with [Spring Boot 2](https://spring.io/projects/spring-boot) and [Apache Maven](https://maven.apache.org/),
with a dependency on [H2] database.


Operations covered,

| Operation       | Description    | [RequestType]:Request URL |
| :------------- | :----------: | :----------- |
| upload the data | Save the uploaded csv file's data  |[POST]:http://localhost:8080/api/medical/upload    |
| Fetch all data   | Get all medical details present in the database |[GET]:http://localhost:8080/api/medical/details |
| Fetch by code   | Get details of the specified medical code |[GET]:http://localhost:8080/api/medical/details/{code} |
| Delete all data   | Delete all the medical details present in the db |[DELETE]:http://localhost:8080/api/medical/wipe |


## Prerequisites

What is needed to start developing and to run the application:

- JDK 11+, for instance [OpenJDK 11](https://openjdk.java.net/projects/jdk/11/)
- Apache Maven 3.3+
- Lombok IDE plugin
- MapStruct IDE plugin
