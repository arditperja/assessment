package com.gerimedica.assessment.web;

import com.gerimedica.assessment.dto.response.BasicDTOResponse;
import com.gerimedica.assessment.dto.response.MedicalDetailListDTOResponse;
import com.gerimedica.assessment.dto.response.SingleMedicalDetailDTOResponse;
import com.gerimedica.assessment.service.MedicalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

@AllArgsConstructor
@RestController
@Api(value = "The medical checkup API")
@RequestMapping(value = "/medical")
public class MedicalResource {

    private final MedicalService medicalService;

    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiOperation(value = "Save the uploaded csv file's data")
    public ResponseEntity<BasicDTOResponse> processUploadFile(@RequestParam MultipartFile file) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getInputStream(), StandardCharsets.UTF_8));
        return ResponseEntity.ok().body(medicalService.saveUploadedFile(bufferedReader));
    }

    @GetMapping(value = "/details")
    @ApiOperation(value = "Get all medical details present in the database")
    public ResponseEntity<MedicalDetailListDTOResponse> getAllMedicalDetails() {
        return ResponseEntity.ok().body(medicalService.getAllMedicalDetails());
    }

    @GetMapping(value = "/details/{code}")
    @ApiOperation(value = "Get details of the specified medical code")
    public ResponseEntity<SingleMedicalDetailDTOResponse> getSpecificMedicalDetail(@PathVariable("code") final String code) {
        return ResponseEntity.ok().body(medicalService.getSpecificMedicalDetail(code));
    }

    @DeleteMapping(value = "/wipe")
    @ApiOperation(value = "Delete all the medical details present in the db")
    public ResponseEntity<BasicDTOResponse> deleteALl() {
        return ResponseEntity.ok().body(medicalService.deleteAll());
    }

}
