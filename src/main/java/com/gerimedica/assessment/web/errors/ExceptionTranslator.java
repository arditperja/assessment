package com.gerimedica.assessment.web.errors;

import com.gerimedica.assessment.dto.response.BasicDTOResponse;
import com.gerimedica.assessment.exception.MedicalServerException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionTranslator {

    @ExceptionHandler(MedicalServerException.class)
    public ResponseEntity<BasicDTOResponse> handleCustomServerException(final MedicalServerException ex) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(BasicDTOResponse.builder().message(ex.getMessage()).build());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<BasicDTOResponse> handleByDefault(final Exception ex) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(BasicDTOResponse.builder().message(MedicalServerException.INTERNAL_ERROR_MESSAGE).build());
    }

}
